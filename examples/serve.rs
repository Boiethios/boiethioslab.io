use warp::Filter;

#[tokio::main]
async fn main() {
    let cors = warp::cors()
        .allow_any_origin()
        .allow_methods(vec!["GET", "POST"]);

    warp::serve(warp::fs::dir("public").with(cors))
        .run(([127, 0, 0, 1], 8080))
        .await;
}
