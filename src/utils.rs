use anyhow::{Context as _, Result};
use pulldown_cmark::{html, Parser};
use std::path::PathBuf;
use tokio::fs;

const INPUT_DIR: &'static str = "data/contexts";

/// Returns the full file path from an input file name.
pub fn input_context_path(name: &str) -> PathBuf {
    PathBuf::from(INPUT_DIR).join(name).with_extension("ron")
}

/// Convert the markdown input to HTML.
pub fn md_to_html(md_input: &str) -> String {
    let parser = Parser::new(md_input);
    let mut html_buf = String::new();

    html::push_html(&mut html_buf, parser);

    html_buf
}

/// Copy the content of a directory to another directory.
//TODO make it recursive
pub async fn copy_dir(src: impl Into<PathBuf>, dst: impl Into<PathBuf>) -> Result<()> {
    let dst = dst.into();

    // Create the dst directory:
    fs::create_dir_all(&dst)
        .await
        .context("Failed to create the output directory")?;

    let mut dirs = fs::read_dir(src.into())
        .await
        .context("Failed to read the source directory")?;

    //TODO concurrency
    while let Some(file) = dirs
        .next_entry()
        .await
        .context("Failed to read a directory entry")?
    {
        let from = file.path();
        let to = dst.join(
            from.file_name()
                .context(format!("{:?} is not a valid file", from))?,
        );
        fs::copy(&from, &to)
            .await
            .context(format!("Failed to copy {:?} to {:?}", from, to))?;
    }

    Ok(())
}

pub use urlize::Urlize;

/// Allows to transform a string to a more URL-friendly string.
mod urlize {
    pub struct Urlize<'a>(std::iter::Peekable<std::str::Chars<'a>>);

    impl<'a> Urlize<'a> {
        pub fn new(s: &'a str) -> Self {
            Urlize(s.chars().peekable())
        }
    }

    impl<'a> Iterator for Urlize<'a> {
        type Item = char;

        fn next(&mut self) -> Option<Self::Item> {
            use std::ops::Not;

            let mut underscore = false;
            while self.0.peek()?.is_alphanumeric().not() {
                underscore = true;
                let _ = self.0.next();
            }
            if underscore {
                Some('_')
            } else {
                self.0.next()
            }
        }
    }

    #[test]
    fn urlize_0() {
        let s = "Hello world!";
        let url: String = Urlize::new(s).collect();

        assert_eq!(url, "Hello_world");
    }

    #[test]
    fn urlize_1() {
        let s = "Hello: this is me";
        let url: String = Urlize::new(s).collect();

        assert_eq!(url, "Hello_this_is_me");
    }
}
