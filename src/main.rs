mod html;
mod sass;
mod utils;

use anyhow::{Context as _, Result};
use std::path::PathBuf;

#[tokio::main]
async fn main() -> Result<()> {
    let out = PathBuf::from("public");
    let mut handles = Vec::new();

    if is_arg("style") {
        handles.push((
            tokio::spawn(sass::generate(out.join("style"))),
            "generate the css files",
        ));
    }
    if is_arg("raw") {
        handles.push((
            tokio::spawn(utils::copy_dir("data/raw", out.clone())),
            "copy the raw files",
        ));
    }
    if is_arg("html") {
        html::generate(out.clone())
            .await
            .context("Failed to generate the html files")?;
        println!("[OK] generate the html files");
    }

    for (handle, msg) in handles {
        handle
            .await
            .context("Failed to join the future")?
            .context(format!("Failed to {}", msg))?;
        println!("[OK] {}", msg);
    }

    Ok(())
}

/// A “feature” is activated either if it's explicitely done, or if there is no parameter given.
fn is_arg(name: &str) -> bool {
    std::env::args().count() <= 1 || std::env::args().find(|arg| arg == name).is_some()
}
