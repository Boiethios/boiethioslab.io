mod layout;
mod page;
mod tera_fn;

mod translator;

use anyhow::{Context as _, Result};
use layout::Layout;
use std::path::Path;
use tera::{Context, Tera};
use tokio::fs;
use translator::Translator;

use page::{blog, cv, index, projects};

const TEMPLATES_PATH: &str = "data/templates/*.tera.html";

/// A page gotten from its context file.
pub async fn generate(out: std::path::PathBuf) -> Result<()> {
    let tera = &mut Tera::new(TEMPLATES_PATH).context("Failed to create the Tera engine")?;
    let langs = ["en", "fr"];
    //TODO: add the projects page:
    let pages = [
        cv::TeraContext::get().await?,
        index::TeraContext::get().await?,
        //projects::TeraContext::get().await?,
        blog::TeraContext::get().await?,
    ];
    let layout = Layout::get()
        .await
        .context(format!("Failed to get the layout"))?;

    tera_fn::register(tera);

    //let articles = blog::Articles::get();

    for &lang in &langs {
        let translator = &Translator::new(lang)
            .context(format!("Failed to get the translator for lang {:?}", lang))?;

        // Create the output directory for the HTML pages:
        fs::create_dir_all(out.clone().join(lang))
            .await
            .context(format!(
                "Failed to create the directory for lang {:?}",
                lang
            ))?;

        // Render and write the pages:
        let layout = layout
            .translate(translator)
            .context(failed_translation("layout", translator.lang))?;
        for page in &pages {
            let page = page
                .translate(translator)
                .context(failed_translation(page.name(), translator.lang))?;
            page::render(page.as_ref(), tera, &layout, lang, out.clone()).await?;
        }
        //todo!("The other pages (blog)")
    }

    for page in pages.iter().map(|p| p.name()) {
        generate_redirect(tera, &out, page)
            .await
            .context("Failed to generate the redirection file")?;
    }

    Ok(())
}

/// Render the page that redirect to the page with the correct lang.
async fn generate_redirect(tera: &mut Tera, out: &Path, page: &str) -> Result<()> {
    let context = {
        let mut context = Context::new();
        context.insert("page", page);
        context
    };

    let out_path = out.join(page).with_extension("html");

    let page_content = tera
        .render("redirect.tera.html", &context)
        .context(format!("Failed to render the redirect page {:?}", page))?;

    fs::write(out_path, page_content)
        .await
        .context(format!("Failed to write the redirect page {:?}", page))
}

fn failed_translation(page: &str, lang: &str) -> String {
    format!(
        "Failed to translate the page {:?} for lang {:?}",
        page, lang
    )
}
