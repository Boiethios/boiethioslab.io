use crate::html::translator::Translator;
use anyhow::Result;

pub struct Layout {
    pub links: navbar::Links,
    pub footer: footer::Footer,
}

impl Layout {
    pub async fn get() -> Result<Self> {
        Ok(Layout {
            links: navbar::Links::get().await?,
            footer: footer::Footer::get().await?,
        })
    }

    pub fn translate(&self, translator: &Translator) -> Result<Self> {
        Ok(Self {
            links: self.links.translate(translator)?,
            footer: self.footer.translate(translator)?,
        })
    }
}

pub mod navbar {
    use anyhow::{bail, Context as _, Result};
    use serde::{Deserialize, Serialize};

    use crate::html::translator::{Translatable, Translator};

    #[derive(Debug, Clone, Serialize, Deserialize)]
    pub struct Link {
        title: Translatable,
        page: String,
        #[serde(default)]
        selected: bool,
        content: Translatable,
    }

    #[derive(Debug, Clone)]
    pub struct Links(pub Vec<Link>);

    impl Links {
        pub async fn get() -> Result<Self> {
            let filepath = crate::utils::input_context_path("navbar");
            let links = tokio::task::spawn_blocking(move || {
                let file = std::fs::File::open(&filepath)
                    .context(format!("Failed to open {:?}", filepath.as_os_str()))?;

                ron::de::from_reader(file)
                    .context(format!("Failed to deserialize {:?}", filepath.as_os_str()))
            })
            .await
            .context("Failed to await future")?
            .context("Failed to deserialize the \"navbar\" context")?;

            Ok(Links(links))
        }

        pub fn select(&self, page: &str) -> Result<Self> {
            let mut result = Links(self.0.clone());

            if let Some(link) = result.0.iter_mut().find(|link| link.page == page) {
                link.selected = true;
            } else {
                bail!("Failed to select the page {:?}", page)
            }

            Ok(result)
        }

        pub fn translate(&self, translator: &Translator) -> Result<Self> {
            let mut s = self.clone();

            for link in &mut s.0 {
                translator.translate(&mut link.title)?;
                translator.translate(&mut link.content)?;
            }

            Ok(s)
        }
    }
}

pub mod footer {
    use anyhow::{Context as _, Result};
    use std::{collections::HashMap, fs::File};

    use crate::html::translator::{Translatable, Translator};

    #[derive(Debug, Clone)]
    pub struct Footer {
        pub sections: HashMap<String, HashMap<String, Translatable>>,
    }

    impl Footer {
        pub async fn get() -> Result<Footer> {
            let filepath = crate::utils::input_context_path("footer");
            let sections = tokio::task::spawn_blocking(move || {
                let file = File::open(&filepath)
                    .context(format!("Failed to open {:?}", filepath.as_os_str()))?;

                ron::de::from_reader(file)
                    .context(format!("Failed to deserialize {:?}", filepath.as_os_str()))
            })
            .await
            .context("Failed to await future")?
            .context("Failed to deserialize the \"footer\" context")?;

            Ok(Footer { sections })
        }

        pub fn translate(&self, translator: &Translator) -> Result<Self> {
            let mut s = self.clone();

            for (_, section) in &mut s.sections {
                for (_, s) in section {
                    translator.translate(s)?;
                }
            }

            Ok(s)
        }
    }
}
