use anyhow::{Context as _, Result};
use serde::{Deserialize, Serialize};
use tera::Context;

use super::Page;
use crate::html::translator::{Translatable, Translator};

pub const NAME: &'static str = "index";

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TeraContext {
    presentation_title: Translatable,
    presentation_content: Translatable,
    hardskills_title: Translatable,
    hardskills_content: Translatable,
    softskills_title: Translatable,
    softskills_content: Translatable,
}

impl Page for TeraContext {
    fn name(&self) -> &str {
        NAME
    }

    fn tera_context(&self) -> Result<Context> {
        Context::from_serialize(self).context("Failed to build the index context")
    }

    fn translate(&self, translator: &Translator) -> Result<Box<dyn Page>> {
        let mut context = self.clone();

        translator.translate(&mut context.presentation_title)?;
        translator.translate(&mut context.presentation_content)?;
        translator.translate(&mut context.hardskills_title)?;
        translator.translate(&mut context.hardskills_content)?;
        translator.translate(&mut context.softskills_title)?;
        translator.translate(&mut context.softskills_content)?;

        Ok(Box::new(Self {
            presentation_content: context.presentation_content.to_html(),
            hardskills_content: context.hardskills_content.to_html(),
            softskills_content: context.softskills_content.to_html(),
            ..context
        }))
    }
}

impl TeraContext {
    pub async fn get() -> Result<Box<dyn Page>> {
        let filepath = crate::utils::input_context_path(NAME);
        let context = tokio::task::spawn_blocking(move || {
            let file = std::fs::File::open(&filepath)
                .context(format!("Failed to open {:?}", filepath.as_os_str()))?;

            ron::de::from_reader::<_, TeraContext>(file)
                .context(format!("Failed to deserialize {:?}", filepath.as_os_str()))
        })
        .await
        .context("Failed to await future")?
        .context("Failed to deserialize the \"index\" context")?;

        Ok(Box::new(context))
    }
}
