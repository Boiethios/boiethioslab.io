use anyhow::{Context as _, Result};
use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use tera::Context;

use super::Page;
use crate::html::translator::Translator;
use crate::utils::md_to_html;

pub const NAME: &'static str = "blog";

const PAT: &str = "private_token=AmPeG6zykNxh1etM-hN3";
const API_BASE: &str = "https://gitlab.com/api/v4/projects/7835068/snippets";

#[derive(Debug, Clone, Serialize)]
pub struct Article {
    id: u32,
    created_at: NaiveDate,
    title: String,
    description: String,
    #[serde(skip_serializing)]
    md_content: String,
    // Temporary:
    url_link: String,
}

#[derive(Debug, Clone, Serialize)]
pub struct TeraContext {
    articles: Vec<Article>,
}

impl Page for TeraContext {
    fn name(&self) -> &str {
        NAME
    }

    fn tera_context(&self) -> Result<Context> {
        Context::from_serialize(self).context("Failed to build the blog context")
    }

    fn translate(&self, translator: &Translator) -> Result<Box<dyn Page>> {
        let _ = translator;
        Ok(Box::new(self.clone()))
    }
}

impl TeraContext {
    pub async fn get() -> Result<Box<dyn Page>> {
        use futures::stream::{StreamExt, TryStreamExt};

        #[derive(Debug, Clone, Serialize, Deserialize)]
        pub struct ArticleDto {
            id: u32,
            #[serde(with = "my_date_format")]
            created_at: NaiveDate, // 2018-08-09T15:14:08.549Z
            title: String,
            description: String,
        }

        let uri = format!("{}?{}", API_BASE, PAT);
        let articles: Vec<ArticleDto> = reqwest::get(&uri)
            .await
            .context("Failed to fetch the articles information")?
            .json()
            .await
            .context("Failed to deserialize the articles information")?;

        async fn fetch_and_write(article: ArticleDto) -> Result<()> {
            let uri = format!("{}/{}/raw?{}", API_BASE, article.id, PAT);
            let result = reqwest::get(&uri)
                .await
                .context(format!("Failed to fetch the article: {}", article.id))?;
            let content = result
                .text()
                .await
                .context(format!("Failed to read the article body: {}", article.id))?;

            let html = md_to_html(&content);
            let filename: String = crate::utils::Urlize::new(&article.title).collect();

            tokio::fs::write(filename, &html)
                .await
                .context(format!("Failed to read the article body: {}", article.id))
        }

        //tokio::stream::iter(articles.clone())
        //    .map(Result::Ok)
        //    .try_for_each_concurrent(4, fetch_and_write)
        //    .await
        //    .context("Failed to fetch and write the articles")?;

        Ok(Box::new(TeraContext {
            articles: articles
                .into_iter()
                .map(|dto| {
                    Article {
                        id: dto.id,
                        created_at: dto.created_at,
                        title: dto.title,
                        description: dto.description,
                        md_content: String::new(),
                        // Temporary:
                        url_link: format!(
                            "https://gitlab.com/Boiethios/blog/-/snippets/{}",
                            dto.id
                        ),
                    }
                })
                .collect(),
        }))
    }
}

mod my_date_format {
    use chrono::NaiveDate;
    use serde::{self, Deserialize, Deserializer, Serializer};

    const FORMAT: &str = "%Y-%m-%d";

    pub fn serialize<S>(date: &NaiveDate, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = format!("{}", date.format(FORMAT));
        serializer.serialize_str(&s)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<NaiveDate, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        let s = &s[0..10];
        NaiveDate::parse_from_str(s, FORMAT).map_err(serde::de::Error::custom)
    }
}

/*
#[tokio::test]
async fn articles_info_can_be_fetched() {
    let context = context().await.unwrap();

    dbg!(context);
}
*/
