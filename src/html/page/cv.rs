use anyhow::{Context as _, Result};
use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use tera::Context;

use super::Page;
use crate::html::translator::{Translatable, Translator};

pub const NAME: &'static str = "cv";

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TeraContext {
    pub xp: Vec<Experience>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Experience {
    from: NaiveDate,
    to: NaiveDate,
    place: String,
    title: Translatable,
    description: Translatable,
    technical: Translatable,
}

impl Page for TeraContext {
    fn name(&self) -> &str {
        NAME
    }

    fn tera_context(&self) -> Result<Context> {
        Context::from_serialize(self).context("Failed to build the CV context")
    }

    fn translate(&self, translator: &Translator) -> Result<Box<dyn Page>> {
        let mut xp = self.clone().xp;

        for exp in &mut xp {
            translator.translate(&mut exp.title)?;
            translator.translate(&mut exp.description)?;
            translator.translate(&mut exp.technical)?;
        }

        Ok(Box::new(TeraContext {
            xp: xp.into_iter().map(Experience::to_html).collect(),
        }))
    }
}

impl Experience {
    fn to_html(self) -> Experience {
        Experience {
            description: self.description.to_html(),
            technical: self.technical.to_html(),
            ..self
        }
    }
}

impl TeraContext {
    pub async fn get() -> Result<Box<dyn Page>> {
        let filepath = crate::utils::input_context_path(NAME);
        let TeraContext { xp, .. } = tokio::task::spawn_blocking(move || {
            let file = std::fs::File::open(&filepath)
                .context(format!("Failed to open {:?}", filepath.as_os_str()))?;

            ron::de::from_reader::<_, TeraContext>(file)
                .context(format!("Failed to deserialize {:?}", filepath.as_os_str()))
        })
        .await
        .context("Failed to await future")?
        .context("Failed to deserialize the \"cv\" context")?;

        Ok(Box::new(TeraContext { xp }))
    }
}
