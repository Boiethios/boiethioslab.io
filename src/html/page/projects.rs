use anyhow::{Context as _, Result};
use serde::{Deserialize, Serialize};
use tera::Context;

use super::Page;
use crate::html::translator::{Translatable, Translator};

pub const NAME: &'static str = "projects";

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TeraContext {
    foo: Translatable,
}

impl Page for TeraContext {
    fn name(&self) -> &str {
        NAME
    }

    fn tera_context(&self) -> Result<Context> {
        Context::from_serialize(self).context("Failed to build the CV context")
    }

    fn translate(&self, translator: &Translator) -> Result<Box<dyn Page>> {
        let mut s = self.clone();

        translator.translate(&mut s.foo)?;

        Ok(Box::new(s))
    }
}

impl TeraContext {
    pub async fn get() -> Result<Box<dyn Page>> {
        let filepath = crate::utils::input_context_path(NAME);
        let context = tokio::task::spawn_blocking(move || {
            let file = std::fs::File::open(&filepath)
                .context(format!("Failed to open: {:?}", filepath.as_os_str()))?;

            ron::de::from_reader::<_, TeraContext>(file)
                .context(format!("Failed to deserialize: {:?}", filepath.as_os_str()))
        })
        .await
        .context("Failed to await future")?
        .context("Failed to deserialize the \"projects\" context")?;

        Ok(Box::new(context))
    }
}
