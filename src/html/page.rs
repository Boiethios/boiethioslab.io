pub mod blog;
pub mod cv;
pub mod index;
pub mod projects;

use super::layout::Layout;
use crate::html::translator::Translator;
use anyhow::{Context as _, Result};
use tera::{Context, Tera};
use tokio::fs;

pub trait Page {
    fn name(&self) -> &str;
    fn tera_context(&self) -> Result<Context>;
    fn translate(&self, translator: &Translator) -> Result<Box<dyn Page>>;
}

/// Render a given page and write it in its file.
pub async fn render(
    page: &dyn Page,
    tera: &mut Tera,
    layout: &Layout,
    lang: &str, //TODO Maybe this parameter isn't mandatory
    out: std::path::PathBuf,
) -> Result<()> {
    let mut context = page
        .tera_context()
        .context(format!("Failed to build the context {:?}", page.name()))?;

    context.insert("navbar_links", &layout.links.select(page.name())?.0);
    context.insert("footer", &layout.footer.sections);
    context.insert("lang", lang);

    let out = out.join(lang).join(page.name()).with_extension("html");
    let page = tera
        .render(&format!("{}.tera.html", page.name()), &context)
        .context(format!("Failed to render the tera page {:?}", page.name()))?;

    fs::write(out, &page)
        .await
        .context(format!("Failed to write the rendered page {:?}", page))
}
