use anyhow::{Context as _, Result};
use serde::Deserialize;
use std::{collections::HashMap, fs::File, path::PathBuf};

pub struct Translator {
    map: HashMap<String, String>,
    pub lang: &'static str,
}

impl Translator {
    pub fn new(lang: &'static str) -> Result<Self> {
        let filepath = PathBuf::from("data/contexts/langs")
            .join(lang)
            .with_extension("ron");
        let file = File::open(&filepath).context(format!("Failed to open {:?}", filepath))?;

        let map =
            ron::de::from_reader(file).context(format!("Failed to deserialize {:?}", filepath))?;

        Ok(Translator { map, lang })
    }

    pub fn translate(&self, s: &mut Translatable) -> Result<()> {
        if let Translatable::Var(var) = s {
            let translated = self
                .map
                .get(var)
                .context(format!(
                    "No such variable {:?} for lang {:?}",
                    var, self.lang
                ))?
                .clone();
            *s = Translatable::Text(translated);
        }

        Ok(())
    }
}

/// This represents a translatable string. The `Text` variant is displayed properly
/// while the `Var` variant returns an error.
#[derive(Debug, Clone, Deserialize)]
pub enum Translatable {
    Text(String),
    Var(String),
}

use std::fmt;

impl fmt::Display for Translatable {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Text(s) => write!(f, "{}", s),
            Self::Var(_var) => Err(fmt::Error),
        }
    }
}

impl Translatable {
    /// Transforms the markdown text to html.
    pub fn to_html(self) -> Self {
        match self {
            Self::Text(s) => Self::Text(crate::utils::md_to_html(&s)),
            var => var,
        }
    }
}

impl serde::Serialize for Translatable {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        match self {
            Self::Text(s) => serializer.serialize_str(s),
            Self::Var(var) => Err(serde::ser::Error::custom(var)),
        }
    }
}
