use chrono::prelude::*;
use std::collections::HashMap;
use std::sync::atomic::{AtomicU32, Ordering};
use tera::{Context, Error, Tera, Value};

macro_rules! register_function {
    ($tera:ident, $name:ident) => {{
        let _ = &$name;
        $tera.register_function(stringify!($name), Box::new($name));
    }};
}

macro_rules! register_filter {
    ($tera:ident, $name:ident) => {{
        let _ = &$name;
        $tera.register_filter(stringify!($name), Box::new($name));
    }};
}

/// Register the needed functions in tera.
pub fn register(tera: &mut Tera) {
    register_function!(tera, date_ser);
    register_function!(tera, years_from);
    register_filter!(tera, eval);
}

fn eval(value: &Value, args: &HashMap<String, Value>) -> tera::Result<Value> {
    const NAME: &str = "eval";
    let tera = {
        let content = value.as_str().ok_or(Error::msg("Value is not a string"))?;
        let mut tera = Tera::default();
        tera.add_raw_template(NAME, content)?;
        register(&mut tera);
        tera
    };
    let context = Context::from_serialize(args)?;

    tera.render(NAME, &context).map(Into::into)
}

fn years_from(args: &HashMap<String, Value>) -> tera::Result<Value> {
    static ID_IDX: AtomicU32 = AtomicU32::new(0);
    let id = ID_IDX.fetch_add(1, Ordering::Relaxed);
    let id = format!("'years-from-{}'", id);

    let date = get_date_arg(args, "date")?;

    let result = format!(
        "<span id={id}></span><script>yearsFrom({id}, new Date({year}, {month}, {day}))</script>",
        id = id,
        year = date.year(),
        month = date.month0(),
        day = date.day()
    );

    Ok(result.into())
}

fn date_ser(args: &HashMap<String, Value>) -> tera::Result<Value> {
    fn to_fr(date: NaiveDate) -> String {
        let month = match date.month() {
            1 => "janv.",
            2 => "févr.",
            3 => "mars",
            4 => "avr.",
            5 => "mai",
            6 => "juin",
            7 => "juill.",
            8 => "août",
            9 => "sept.",
            10 => "oct.",
            11 => "nov.",
            12 => "déc.",
            _ => unreachable!("A month should range from 1 to 12"),
        };

        format!("{} {}", month, date.year())
    }
    fn to_en(date: NaiveDate) -> String {
        let month = match date.month() {
            1 => "Jan",
            2 => "Feb",
            3 => "Mar",
            4 => "Apr",
            5 => "May",
            6 => "Jun",
            7 => "Jul",
            8 => "Aug",
            9 => "Sep",
            10 => "Oct",
            11 => "Nov",
            12 => "Dec",
            _ => unreachable!("A month should range from 1 to 12"),
        };

        format!("{} {}", month, date.year())
    }

    let lang = args
        .get("lang")
        .and_then(Value::as_str)
        .ok_or(Error::from("Failed to get \"lang\" in the date formatter"))?;
    let date = get_date_arg(args, "date")?;

    match lang {
        "en" => Ok(Value::String(to_en(date))),
        "fr" => Ok(Value::String(to_fr(date))),
        _ => Err(Error::from(format!("Invalid lang {:?}", lang))),
    }
}

fn get_date_arg(args: &HashMap<String, Value>, name: &str) -> tera::Result<NaiveDate> {
    args.get(name)
        .and_then(Value::as_str)
        .ok_or(Error::from(format!("Failed to get the date {:?}", name)))
        .map(|s| NaiveDate::parse_from_str(s, "%Y-%m-%d"))?
        .map_err(|e| Error::from(format!("Failed to parse the date {:?}: {}", name, e)))
}
