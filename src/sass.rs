//! Compiles all the scss files.

use anyhow::{Context as _, Result};
use rsass::{compile_scss_file, OutputStyle};
use std::{convert::AsRef, ffi::OsStr, ops::Not};
use tokio::fs;

pub async fn generate(out: std::path::PathBuf) -> Result<()> {
    let mut input_dir = fs::read_dir("./data/styles")
        .await
        .context("Failed to read the styles directory")?;

    fs::create_dir_all(out.clone())
        .await
        .context("Failed to create the style directory")?;

    while let Some(entry) = input_dir
        .next_entry()
        .await
        .context("Failed to read a directory entry")?
    {
        let path = entry.path();

        if let Some("scss") = path
            .extension()
            .map(OsStr::to_string_lossy)
            .as_ref()
            .map(|s| s.as_ref())
        {
            let stem = path.file_stem().context(format!(
                "Failed to get the file name from the entry {:?} (no file name)",
                path
            ))?;
            if stem.to_string_lossy().starts_with("_").not() {
                let css = compile_scss_file(&path, OutputStyle::Expanded)
                    .context(format!("Failed to compile the scss file {:?}", path))?;
                let out = out.clone().join(stem).with_extension("css");

                fs::write(&out, css)
                    .await
                    .context(format!("Failed to write the css file {:?}", out))?;
            }
        }
    }

    Ok(())
}
