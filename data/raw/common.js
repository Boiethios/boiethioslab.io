function yearsFrom(id, date) {
    let elem = document.getElementById(id);
    let diff = Date.now() - date.getTime();
    let years = diff / (1000 * 3600 * 24 * 365);

    elem.innerText = Math.trunc(years);
}