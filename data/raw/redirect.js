const l = (navigator.language || navigator.browserLanguage).substring(0, 2).toLowerCase() === "fr"
    ? "fr"
    : "en";

console.debug("lang:", l);
window.location.replace(`/${l}${window.location.pathname}`);
